Wealth Scale VR 

Requirments:
VR Headset
VR Controllers
VR link cord


How to play the demo:
1.Put on VR headset
2.Connect headset to computer with link cord
3.Make sure link software(in this case Occulus link) is enabled
4.On your computer navigate to the VR demo folder, run Computer Graphics application


Controls:
Use the left thumb stick to move back and foward
Use the right thumb stick to move left and right
Use the bottom trigers on either controllers to pick up items in the demo
